package com.example.listadetarefas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import android.widget.ArrayAdapter
import androidx.room.Room
import com.example.listadetarefas.db.AppDatabase
import com.example.listadetarefas.db.dao.TaskDao
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var taskDao: TaskDao
    lateinit var adapter: ArrayAdapter<Task>
    var taskEditing: Task? = null
    var taskRemoving: Task? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db =
            Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java,
                "tarefa.db"
            )
                .allowMainThreadQueries()
                .build()
           taskDao = db.taskDao()

        btSalvar.setOnClickListener { save() }

        btDeletar.setOnClickListener { removeTask() }

        listaTarefas.setOnItemClickListener { _, _, pos, _ ->
            editTask(getTask(pos))
        }

        listaTarefas.setOnItemLongClickListener { _, _, pos, _ ->
            completeTask(getTask(pos))
            true
        }

        LoadTask()
    }

    private fun editTask(task:Task) {
        taskEditing = task
        taskRemoving = task
        txtTituloInput.setText(task.title)
        txtDercricaoInput.setText(task.description)
        btDeletar.visibility= View.VISIBLE
    }

    private fun completeTask(task:Task) {
        task.status = resources.getString(R.string.status_task_done)
        taskDao.update(task)
        clean()
        LoadTask()

    }

    private fun save() {
        val titulo = txtTituloInput.text.toString()
        val descricao = txtDercricaoInput.text.toString()

        if(taskEditing != null) {
            taskEditing?.let { task ->
                task.title = titulo
                task.description = descricao
                taskDao.update(task)
            }
        }else{
            val status = resources.getString(R.string.status_task_not_done)

            val task = Task(titulo, descricao, status)

            taskDao.insert(task)

        }
        clean()
        LoadTask()
    }

    fun removeTask() {
        if (taskRemoving != null) {
            taskDao.delete(taskRemoving!!)
            LoadTask()
        }
        clean()
    }

    private fun LoadTask() {
        val tasks = taskDao.getAll()

        adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            tasks
        )
        listaTarefas.adapter = adapter
    }

    fun clean() {
        txtTituloInput.setText("")
        txtDercricaoInput.setText("")
        btDeletar.visibility = View.INVISIBLE
        taskRemoving = null
        taskEditing = null
    }

    private fun getTask(pos:Int) = adapter.getItem(pos) as Task
}
