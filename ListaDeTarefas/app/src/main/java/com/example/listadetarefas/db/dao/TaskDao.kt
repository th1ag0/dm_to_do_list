package com.example.listadetarefas.db.dao

import androidx.room.*
import com.example.listadetarefas.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM tarefas")
    fun getAll(): List<Task>

    @Insert
    fun insert(tarefa: Task)

    @Update
    fun update(tarefa: Task)

    @Delete
    fun delete(tarefa: Task)
}