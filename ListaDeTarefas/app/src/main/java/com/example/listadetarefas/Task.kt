package com.example.listadetarefas

import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
data class Task(
    var title:String,
    var description:String,
    var status:String = Resources.getSystem().getString(R.string.status_task_not_done)

) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


    val taskString get() = "[${status}] Titulo: ${title}, Descrição: ${description}"

    override fun toString()= taskString
}