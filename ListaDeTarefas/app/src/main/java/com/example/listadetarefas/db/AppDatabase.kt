package com.example.listadetarefas.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.listadetarefas.Task
import com.example.listadetarefas.db.dao.TaskDao

@Database(version = 1, entities = arrayOf(Task::class))
abstract class AppDatabase:RoomDatabase() {
    abstract fun taskDao():TaskDao
}